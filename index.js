let users = ["Dwayne Johnson","Steve Austin","Kurt Angle","Dave Bautista"];

console.log("Original Array:")
console.log(users);


/*
    [Function without return]
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/

function addElement(index){
	users[users.length] = index;
    console.log(users);
}
addElement('John Cena');

/*  
    [Function with return]
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

function displayName(index){
	return users[index];
}
let itemFound = displayName(2);
console.log(itemFound);


/*  
    
    [Function with return]
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/

    function removeLastItem(){  
        let lastItem = users[users.length-1]; 
        users.length--;
        return lastItem;
    }
    console.log(removeLastItem());
    console.log(users);


/*
    [Function without return]
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/

    function reassignLastElement(update, index){
         users[index] = update;
         console.log(users);
    }

    reassignLastElement('Triple H', users.length-1);

/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

    function deleteAllElements(){
        users.length = users.length-4;
        console.log(users);
    }

    deleteAllElements();

/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/

function checkArr(){
    if(users.length > 0){
        console.log("False");
    }

    else{
        console.log("True");
    }
}

let isUsersEmpty = checkArr();
console.log(isUsersEmpty);